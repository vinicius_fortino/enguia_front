let scope = this;

//Callback Google
let onSuccess = function (googleUser) {
    let idToken = gapi.auth2
        .getAuthInstance()
        .currentUser.get()
        .getAuthResponse().id_token;
    let profile = googleUser.getBasicProfile();
    let Usuario = {
        googleid: profile.getId(),
        email: profile.getEmail(),
        avatar: profile.getImageUrl(),
        nome: profile.getName(),
        idToken: idToken
    };
    scope.$http
        .post(scope.$config("API_URL") + "/usuario/verificar", {
            email: Usuario.email
        })
        .then(
            response => {
                if (response.body.existe) {
                    this.notifica("Usuário já existe, por favor faça o login.");
                } else {
                    router.push({
                        name: "informacoespessoais",
                        params: {
                            Usuario: Usuario
                        }
                    });
                }
            },
            err => {
                console.log(err);
            }
        );
};

//Callback de falha do Google
let onFailure = function () {
    console.log("onFailure");
};

//função que é chamada quando a sdk do google terminar de carregar
let api_ready = function () {
    gapi.load("auth2", function () {
        let auth2 = gapi.auth2.init({
            client_id:
                "801405594596-slid9lhiblg2f4mmjcplh0rtrqt47rqd.apps.googleusercontent.com",
            cookiepolicy: "single_host_origin"
        });
        auth2.attachClickHandler(
            document.getElementById("googleSignin"),
            {},
            onSuccess,
            onFailure
        );
    });
};

//injeta o javscript na página, e assim que terminar invoca a função api_ready
document.head.append(
    Object.assign(document.createElement("script"), {
        src: "https://apis.google.com/js/api.js",
        onload: api_ready
    })
);

//Facebook Login
window.fbAsyncInit = function () {
    FB.init({
        appId: "644455415905944",
        cookie: true,
        xfbml: true,
        version: "v3.0"
    });

    FB.AppEvents.logPageView();
};

(function (d, s, id) {
    var js,
        fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs)
});

export function facebookAuth() {
    let facebookCallback = response => {

        let facebookData = response;
        facebookData.facebookid = facebookData.id;
        facebookData.nome = facebookData.name;
        delete facebookData.id;
        this.$http
            .post(this.$config("API_URL") + "/usuario/verificar", {
                email: response.email
            })
            .then(
                response => {
                    if (response.body.existe) {
                        this.notifica("Usuário já existe, por favor faça o login.");
                    } else {
                        router.push({
                            name: "informacoespessoais",
                            params: {
                                Usuario: facebookData
                            }
                        });
                    }
                },
                err => {
                    console.log(err);
                }
            );
    };
    FB.login(
        function (responseLogin) {
            if (responseLogin.status === "connected") {
                FB.api("/me?fields=id,name,email", function (response) {
                    response.accessToken = responseLogin.authResponse.accessToken;
                    facebookCallback(response);
                });
            }
        },
        { scope: "public_profile, email" }
    );
}