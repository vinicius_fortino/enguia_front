import { HTTP_STATUS } from "@/http_constants";
import { CONSTANTS } from "@/constants";
import { authHeader } from "@/helpers";
import router from "@/router";

const axios = require('axios');
let client = axiosConfig();

export const defaultService = { login, post, get };

// Confgiure axios
function axiosConfig() {
    return axios.create({
        baseURL: CONSTANTS.API.ENV.filter(x => x.TO == "PROD")[0].BASE_URL,
        headers: authHeader()
    });
}

// LOGIN
async function login(loginRequest: any) {
    console.log("defaultservice login", loginRequest)
    return await client.post(CONSTANTS.API.USER.LOGIN, loginRequest)
        .then(response => {
            response = JSON.parse(response);
            if (!response.error) {
                console.log("response.data", response.data);
                localStorage.setItem("enguiauser", JSON.stringify(response.data));
                client = axiosConfig();
                return JSON.parse(response);
            } else {
                throw response.message;
            }
        });
}

// GET
async function get(url: string) {
    return await client.get(url).then(response => { return response.data });
}

// POST
async function post(url: string, object: any) {
    return await client.post(url, object).then(response => { return response.data });
}

// Interceptors
client.interceptors.response.use(
    (response) => {
        let tokensessao = localStorage.getItem('tokensessao');
        if (tokensessao) {
            response.headers.set('authorization', 'Bearer ' + tokensessao);
        }
        return response;
    },
    (err) => {
        if (err.response.status == HTTP_STATUS.UNAUTHORIZED) {
            localStorage.clear();
            router.push({
                name: "login"
            });
        }
        return Promise.reject(JSON.stringify(err.response.data));
    }
);
// Vue.http.interceptors.push(function (request, next) {
//     let tokensessao = localStorage.getItem('tokensessao');
//     if (tokensessao) {
//       request.headers.set('authorization', 'Bearer ' + tokensessao);
//     }

//     next((response) => {
//       if (response.status == 401) {
//         localStorage.clear();
//         router.push({
//           name: "login"
//         });
//       }
//     });
//   })
