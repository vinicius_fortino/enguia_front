export const CONSTANTS = {
    API: {
      ENV: [
        { TO: "TEST", BASE_URL: "http://localhost:3000", VERSION: "" },
        { TO: "PROD", BASE_URL: "http://enguiabackend.herokuapp.com/api/", VERSION: "" },
      ],
      USER: { LOGIN: "auth/login", GET_USER: "/usuario/get", CHECK_ACCOUNT: "/usuario/verificar", CREATE_ACCOUNT: "/usuario/create" },
    },
  };
  