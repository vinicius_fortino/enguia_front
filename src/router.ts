import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Signin from './views/Signin.vue';
import InformacoesPessoais from './views/InformacoesPessoais.vue';

Vue.use(Router);

export const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/signin/:register',
      name: 'signin',
      component: Signin,
    },
    {
      path: '/informacoesPessoais',
      name: 'informacoesPessoais',
      component: InformacoesPessoais,
    },
    // otherwise redirect to home
    { path: "*", redirect: "/" }
  ],
});


router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ["/", "/signin/0", "/signin/1", "/informacoespessoais"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem("user");

  if (to.name) {
  }

  if (authRequired && !loggedIn) {
    return next("/");
  }

  next();
});

export default router;