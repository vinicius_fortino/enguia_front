export const alert = {
    namespaced: true,
    state: { type: null, message: null },
    actions: {
        success({ commit }, message) {
            commit("success", message);
            setTimeout(function () {
                commit("clear", message);
            }, 3000);
        },
        error({ commit }, message) {
            commit("error", message);
            setTimeout(function () {
                commit("clear", message);
            }, 3000);
        },
        clear({ commit }, message) {
            commit("clear", message);
        }
    },
    getters: {
        alert: state => {
            return state;
        }
    },
    mutations: {
        success(state, message) {
            state.type = "alert-success";
            state.message = message;
        },
        error(state, message) {
            state.type = "alert-danger";
            state.message = message;
        },
        clear(state) {
            state.type = null;
            state.message = null;
        }
    }
};
