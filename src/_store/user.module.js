import { defaultService } from "../services";
import { CONSTANTS } from "@/constants";
import router from '../router';

const userName = JSON.parse(localStorage.getItem("nomeusuario"));
const etapacadastro = JSON.parse(localStorage.getItem("etapacadastro"));
const token = JSON.parse(localStorage.getItem("tokensessao"));

const initialState = userName
    ? {
        user: {
            name: userName,
            etapacadastro: etapacadastro,
            token: token
        }, preUser: null
    }
    : { user: null, preUser: null };

export const users = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ dispatch, commit }, userPost) {
            defaultService
                .login(userPost)
                .then(
                    res => {
                        commit("user", userPost);
                    },
                    error => {
                        dispatch("alert/error", error, { root: true });
                    }
                );
        },
        checkAccount({ dispatch, commit }, userPost) {
            defaultService
                .post(CONSTANTS.API.USER.CHECK_ACCOUNT, userPost)
                .then(
                    res => {
                        commit("preUser", userPost)
                        console.log("userPost", userPost)
                        router.push('/informacoespessoais');
                    },
                    error => {
                        dispatch("alert/error", error.message, { root: true });
                    }
                );
        },
        register({ dispatch, commit }, userPost) {
            defaultService
                .post(CONSTANTS.API.USER.CREATE_ACCOUNT, userPost)
                .then(
                    res => {
                        commit("login", res)
                    },
                    error => {
                        dispatch("alert/error", error.message, { root: true });
                    }
                );
        },
        getUser({ dispatch, commit }, userPost) {
            defaultService
                .get(CONSTANTS.API.USER.GET_USER)
                .then(
                    res => {
                        commit("login", res)
                    },
                    error => {
                        dispatch("alert/error", error.message, { root: true });
                    }
                );
        }
    },
    getters: {
        user: state => {
            return state.user;
        },
        preuser: state => {
            console.log("state", state)
            return state.preUser;
        },
    },
    mutations: {
        login(state, user) {
            state.user = user;
        },
        preUser(state, preUser) {
            state.preUser = preUser;
        },
    }
};
